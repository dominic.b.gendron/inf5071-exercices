# Laboratoire 6: Matériaux 3D

## 1 - Éditeur de noeuds

Jouez avec l'éditeur de noeuds disponible dans Blender. Il existe plusieurs
tutoriels sur le sujet. Vous pouvez par exemple consulter celui disponible à
https://www.youtube.com/watch?v=JTYtSmbg_mc.

## 2 - Cuisiner une carte de normales

- Regardez d'abord le tutoriel suivant, qui explique comment générer une carte
  de normales: https://www.youtube.com/watch?v=0r-cGjVKvGw
- Construisez le modèle de votre choix d'abord avec un bas niveau de détails
  (*low poly*) dans Blender. Donnez-lui un nom significatif en indiquant qu'il
  s'agit du modèle de basse complexité (par exemple `ModelLowPoly`). Ça peut
  être une bonne idée de construire un modèle qui sera utile dans votre projet
  de session.
- Faites un développement UV de votre modèle de basse complexité.
- Ensuite, dupliquez votre modèle et ajoutez-lui un grand niveau de détails.
  Donnez le même nom à votre modèle, mais en indiquant qu'il a une haute
  complexité (par exemple `ModelHighPoly`). Vous pouvez par exemple ajouter un
  modificateur de déplacement, ou encore sculpter les détails).
- Faites un développement UV de votre modèle de haute complexité.
- En suivant les instructions du tutoriel mentionné plus haut, générez une
  carte de normales de votre modèle de haute complexité sur celui de basse
  complexité. Sauvegardez le résultat dans une image.
- Ensuite, ouvrez votre moteur de jeu préféré et chargez le modèle de basse
  complexité. Ajoutez-lui la carte de normales associée.

## 3 - Shader générique

Familiarisez-vous avec le nouveau *principled shader* de Blender. Commencez par
consulter le tutoriel https://www.youtube.com/watch?v=4H5W6C_Mbck.

Malheureusement, il n'y a actuellement pas de moyen automatique d'exporter ce
type de matériaux dans Unity ou dans Godot.
