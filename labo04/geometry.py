from math import sqrt

class Point3D(object):
    r"""
    A 3D point.
    """

    def __init__(self, x, y, z):
        r"""
        Creates a 3D point with the given coordinates.

        >>> point = Point3D(4.0, 5.0, -2.0)
        """
        self.x = x
        self.y = y
        self.z = z

    def __repr__(self):
        r"""
        Returns a string representation of self.

        >>> Point3D(4.0, 5.0, -2.0)
        Point3D(4.0, 5.0, -2.0)
        """
        return 'Point3D(%s, %s, %s)' % (self.x, self.y, self.z)

    def __add__(self, vector):
        r"""
        Returns the point obtained by translating self by vector.

        >>> Point3D(4.0, 5.0, -2.0) + Vector3D(1.0, -1.0, 2.0)
        Point3D(5.0, 4.0, 0.0)
        """
        raise NotImplemented

    def __sub__(self, other):
        r"""
        Returns the vector from self to other.

        >>> Point3D(4.0, 5.0, -2.0) - Point3D(1.0, -1.0, 2.0)
        Vector3D(3.0, 6.0, -4.0)
        """
        raise NotImplemented

class Vector3D(object):

    def __init__(self, x, y, z):
        r"""
        Creates an instance of 3D vector.

        >>> v = Vector3D(1.0, 2.0, -3.0)
        """
        self.x = x
        self.y = y
        self.z = z

    def __repr__(self):
        r"""
        Returns a string representation of self.

        >>> Vector3D(1.0, 2.0, -3.0)
        Vector3D(1.0, 2.0, -3.0)
        """
        return 'Vector3D(%s, %s, %s)' % (self.x, self.y, self.z)

    def __rmul__(self, scalar):
        r"""
        Multiplies self by the given scalar.

        >>> 4.0 * Vector3D(1.0, 2.0, -3.0)
        Vector3D(4.0, 8.0, -12.0)
        """
        raise NotImplemented

    def is_zero(self):
        r"""
        Returns True if and only if self is the null vector.

        >>> Vector3D(0.0, 0.0, 0.0).is_zero()
        True
        >>> Vector3D(1.0, 0.0, 0.0).is_zero()
        False
        """
        raise NotImplemented

    def has_same_direction(self, other, and_orientation=False):
        r"""
        Returns True if and only if self and other are in the same directions.

        If ``and_orientation`` is set to True, check also if the vectors point
        in the same direction.

        >>> Vector3D(1.0, 2.0, 3.0).has_same_direction(Vector3D(2.0, 4.0, 6.0))
        True
        >>> Vector3D(1.0, 2.0, 3.0).has_same_direction(Vector3D(-3.0, -6.0, -9.0))
        True
        >>> Vector3D(1.0, 2.0, 3.0).has_same_direction(Vector3D(1.0, 3.0, 2.0))
        False
        >>> Vector3D(0.0, 0.0, 0.0).has_same_direction(Vector3D(1.0, 3.0, 1.0))
        True
        """
        raise NotImplemented

    def dot_product(self, other):
        r"""
        Returns the dot product of self with other.
        >>> Vector3D(0.0, 0.0, 0.0).dot_product(Vector3D(1.0, 3.0, 1.0))
        0.0
        >>> Vector3D(1.0, 2.0, 3.0).dot_product(Vector3D(-7.0, 6.0, 2.0))
        11.0
        """
        raise NotImplemented

    def square_norm(self):
        r"""
        Returns the square norm of self.

        >>> Vector3D(1.0, 2.0, 3.0).square_norm()
        14.0
        >>> Vector3D(0.0, 0.0, 0.0).square_norm()
        0.0
        """
        raise NotImplemented

    def norm(self):
        r"""
        Returns the norm of self.

        >>> Vector3D(1.0, 2.0, 3.0).norm() == sqrt(14.0)
        True
        >>> Vector3D(0.0, 0.0, 0.0).norm()
        0.0
        """
        raise NotImplemented

    def cross_product(self, other):
        r"""
        Returns the cross product of self with other.

        >>> Vector3D(1.0, 2.0, 3.0).cross_product(Vector3D(-1.0, 2.0, 1.0))
        Vector3D(-4.0, -4.0, 4.0)
        >>> Vector3D(0.0, 0.0, 0.0).cross_product(Vector3D(-1.0, 2.0, 1.0)).is_zero()
        True
        """
        raise NotImplemented

class Line3D(object):

    def __init__(self, point, direction):
        r"""
        Creates an instance of a 3D line.

        >>> line = Line3D(Point3D(0.0, 0.0, 0.0), Vector3D(1.0, 2.0, 3.0))
        """
        self.point = point
        self.direction = direction

    def __repr__(self):
        r"""
        Returns a string representation of self.

        >>> Line3D(Point3D(0.0, 0.0, 0.0), Vector3D(1.0, 2.0, 3.0))
        Line3D of direction Vector3D(1.0, 2.0, 3.0) passing through Point3D(0.0, 0.0, 0.0)
        """
        return 'Line3D of direction %s passing through %s' %\
               (self.direction, self.point)

    def __contains__(self, point):
        r"""
        Returns True if and only if the given point lies on the line.

        >>> line = Line3D(Point3D(0.0, 0.0, 0.0), Vector3D(1.0, 2.0, 3.0))
        >>> Point3D(-1.0, -2.0, -3.0) in line
        True
        >>> Point3D(0.0, 0.0, 0.0) in line
        True
        >>> Point3D(1.0, 3.0, 2.0) in line
        False
        """
        raise NotImplemented

    def intersection_with(self, other):
        r"""
        Returns the intersection of self with other.

        There are three possible scenarios:

        - The two lines coincides, so the intersection is a line;
        - The two lines intersect in a single point;
        - The two lines do not intersect.

        >>> point1 = Point3D(4.0, 5.0, -2.0)
        >>> point2 = Point3D(6.0, 5.0, 2.5)
        >>> point3 = Point3D(-2.0, 1.0, 4.0)
        >>> vector1 = Vector3D(1.0, 2.0, 3.0)
        >>> vector2 = Vector3D(-1.0, 2.0, -1.5)
        >>> vector3 = Vector3D(-1.0, 2.0, -4.0)
        >>> line1 = Line3D(point1, vector1)
        >>> line2 = Line3D(point2, vector2)
        >>> line3 = Line3D(point3, vector3)
        >>> line1.intersection_with(line1)
        Line3D of direction Vector3D(1.0, 2.0, 3.0) passing through Point3D(4.0, 5.0, -2.0)
        >>> line1.intersection_with(line2)
        Point3D(5.0, 7.0, 1.0)
        >>> line1.intersection_with(line3) is None
        True
        """
        raise NotImplemented
