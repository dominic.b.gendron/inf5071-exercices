# Laboratoire 4: Lumière

**Note**: Les exercices suggérés se font à l'aide de Python, mais vous pouvez
utiliser un autre langage de programmation, comme C++, si vous préférez.

## 1 - Points et droites

Complétez le fichier [geometry.py](geometry.py) fourni dans ce dépôt. Notez
qu'il est très similaire à celui utilisé dans le travail pratique 1.

## 2 - Segments

Étendez le module `geometry.py` pour qu'il supporte une classe représentant un
segment de droite, qui peut être construit à partir de deux points. En
particulier, vous devez supporter les opérations suivantes:

- Décider si un point se trouve sur le segment;
- Décider si deux segments s'intersectent;
- Retourner le point à l'intersection de deux segments, s'il existe;
- Retourner le point à l'intersection d'un segment et d'une droite, s'il
  existe.

## 3 - Demi-droites

De la même façon qu'à la question précédente, étendez le module `geometry.py`
pour qu'il supporte le même type de requêtes pour des demi-droites (en anglais,
on dit *half-line*).

## 4 - Lumières dans Blender

Construisez une simple scène en Blender dans laquelle vous utilisez au moins
une fois chaque type de lumière (*Point*, *Sun*, *Spotlight*, *Hemi* et
*Area*). N'hésitez pas à ajouter une couleur à certaines de ces lumières. Par
exemple, vous pouvez faire une scène de fin de journée (le soleil est donc
d'intensité très faible, ce qui justifie l'utilisation d'autres lumières). Ne
faites pas de modèles complexes, juste les maillages de base suffisent (sphère,
cube, plan, etc.)

Ensuite, exportez votre scène dans votre moteur de jeu préféré. Vérifiez si
tous vos paramètres ont été exportés correctement.

## 5 - Tore

Implémentez une classe `Torus` en Python qui permet de représenter un tore de
centre $`C`$, de rayon majeur $`R`$ et de rayon mineur $`r`$. En particulier,
vous devriez supporter les opérations suivantes:

- Décider si un point se trouve sur la surface du tore;
- Décider si un point se trouve à l'intérieur du tore;
- Étant donné un point qui se trouve sur la surface du tore, donner un vecteur
  normal au tore en ce point (*Note*: vous pouvez utilisez les assertions pour
  vous assurer que le point se trouve bien sur le tore).
