# Laboratoire 2: Textures

## 1 - Développement de modèles

À l'aide de Blender, construisez un modèle de complexité moyenne, c'est-à-dire
plus complexe que les maillages de base, mais pas trop complexe non plus.
Idéalement, construisez un object qui possédera au moins deux matériaux
différents. Par exemple, il pourrait s'agir d'un coffre en bois avec des
bordures de métal. Vous pouvez aussi reprendre un des modèles que vous avez
créés dans le premier laboratoire. Ne passez pas trop de temps sur cette partie
à moins que vous souhaitiez vous entraîner à nouveau à modéliser un objet 3D.

Ensuite, identifiez des coutures (*seams*) bien choisies afin de développer
(*uv-unwrap*) votre modèle. Il est préférable minimalement d'ajouter une
couture lorsqu'il y a changement de matériau (par exemple, en reprenant l'idée
du coffre, il devrait y avoir une couture le long de la transition bois-métal)
de façon à obtenir un développement dont il est facile de retrouver à quelle
face il correspond.

Finalement, développez votre modèle. N'oubliez pas de sauvegarder le fichier
`.blend`.

## 2 - Plaquage d'une texture

Une fois votre modèle développé, sauvegardez son développement dans un fichier
PNG. En Blender, il s'agit, dans l'éditeur d'images/UV, de sélectionner `UVs`
et ensuite `Export UV Layout`.

Ouvrez l'image PNG dans votre éditeur d'images 2D préféré (par exemple Krita ou
Gimp). Ajoutez une nouvelle couche (*layer*) et dessinez une image représentant
le matériau souhaité selon la face développée correspondante. Ne vous appliquez
pas trop, le but n'étant pas d'obtenir un joli résultat, mais de vous assurez
que vous êtes en mesure de reproduire les étapes. Ensuite, sauvegardez l'image
de la texture sous format PNG (en cachant la couche qui contient le contour des
formes, puisque vous ne voudrez pas qu'elle apparaisse).

Retournez dans Blender et appliquez la texture que vous venez de dessiner à
votre modèle. Admirez le résultat.

## 3 - Peinture d'un modèle 3D

De façon alternative, il est possible de "peinturer" un modèle 3D directement
dans Blender. Après avoir fait le développement de votre modèle, sélectionnez
le mode `Paint`. Blender vous avertira qu'il n'y a pas de texture associée à
votre object, mais vous pourrez en créer une, qui sera initialement
complètement noire.

Amusez-vous à peinturer votre modèle, en prenant bien soin d'ouvrir à côté
l'éditeur d'images/UV, afin de voir l'évolution de la texture au fur et à
mesure que vous peinturez votre objet. N'oubliez pas de sauvegarder non
seulement le fichier `.blend`, mais également l'image PNG qui contient la
texture que vous avez créée.

## 4 - Exportation/importation d'un modèle

Exportez votre modèle au format DAE (pour Godot) ou FBX (pour Unity). Notez que
le développement que vous avez fait et le chemin vers l'image PNG contenant la
texture sont identifiés. Assurez-vous que le chemin en question soit relatif et
donc correct lorsque vous ferez l'importation à l'aide de Godot/Unity.

Importez ensuite le modèle dans le moteur de jeu que vous avez choisi.
Assurez-vous que la texture est correctement importée aussi dans le champ
*Diffuse*.

## 5 - Génération d'images avec Pillow

La bibliothèque [Pillow](https://pillow.readthedocs.io/en/latest/index.html)
est une bibliothèque Python qui permet de manipuler des images.

En utilisant cette bibliothèque, générez de façon procédurale une texture de
briques similaire à celle présentée ci-bas.

![Une texture de briques](briques.png)

Je vous recommande de consulter la page de référence sur le module
[ImageDraw](https://pillow.readthedocs.io/en/latest/reference/ImageDraw.html)
pour vous familiariser avec les fonctions disponibles.

Voici les paramètres que j'ai utilisés pour représenter le mur, afin qu'il soit
plus facile d'obtenir une texture périodique (*seamless*) :

- `xmargin` : la demi-longueur horizontale entre deux briques;
- `ymargin` : la demi-longueur verticale entre deux briques;
- `xwidth` : la largeur horizontale entre le début de deux briques (donc la
  largeur d'une brique est `xwidth - xmargin`);
- `ywidth` : la hauteur entre le début de deux briques (donc la hauteur d'une
  brique est `ywidth - ymargin`.
