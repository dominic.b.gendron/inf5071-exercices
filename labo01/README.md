# Laboratoire 1: Environnement

## 1 - Blender

Familiarisez-vous avec l'interface de base de Blender en testant les
fonctionnalités suivantes.

- *Scène* :

  * *Zoomer* vers l'avant et vers l'arrière de la scène;
  * *Déplacer* le champ de vision haut/bas/gauche/droite (panning);
  * *Rotation* de la scène;
  * Différentes *vues* de la scène (haut/bas/devant/derrière/gauche/droite);
  * Changement de *projection* perspective/orthographique (NUMPAD-5).

- *Manipulation* d'objets :
  
  * *Ajout* d'un objet (SHIFT-A);
  * *Suppression* d'un objet (touche X);
  * *Déplacement* d'un objet avec les flèches ou en mode *grab* (touche G);
  * *Redimensionnement* d'un objet (touche S);
  * *Rotation* d'un objet (touche R).

- *Sélection* d'objets :
  
  * *Un* à la fois (bouton gauche de la souris);
  * *Cumulative* (en maintenant SHIFT)
  * *Tous* les objets ou *aucun* objet (touche A);
  * Par *boîte* (touche B);
  * *Circulaire* (touche C, puis roulette de la souris ou +/- pour
    redimensionner).

- *Manipulation de maillages* :

  * *Changement* de mode objet/édition (touche TAB);
  * *Type* de sélection sommet/arête/face;
  * Les sommets, arêtes et faces peuvent être déplacés, redimensionnés et subir
    une rotation;
  * *Extrusion* (touche E), qui peut être suivie d'une contrainte (touches X, Y
    ou Z) ou d'un redimensionnement (touche S), qui peut lui-même être
    contraint (touches X, Y ou Z);
  * *Subdivisions*, qui peuvent s'effectuer sur des arêtes ou des faces;
  * *Création* d'une nouvelle arête (touche F) en sélectionnant deux sommets;
  * *Création* d'une nouvelle face (touche F) en sélectionnant un cycle de
    sommets;

- *Caméra et rendu* :

  * *Dimensions* de l'image;
  * *Qualité* de l'image (en faisant varier la compression);
  * *Position* de la caméra;
  * *Type* de caméra (orthographique ou perspective);
  * Modifiez l'arrière-plan pour qu'il soit *transparent* (dans l'onglet
    *Render*, section *Shading*).

## 2 - Modélisation d'une scène complète

Dans Blender, modélisez une scène en y intégrant les éléments suivants:

- Un terrain plus ou moins accidenté (utilisez votre imagination), ainsi que
  des modifieurs (par exemple de *déplacement*);
- Un arbre;
- Une petite maison;
- Des pierres;

Afin de donner un aspect plus intéressant à vos objets, ajoutez-leur des
matériaux avec couleur simple (*Diffuse*). N'hésitez pas à ajouter d'autres
éléments en laissant libre cours à votre imagination.

Faites un rendu de votre scène et admirez le résultat. Regardez votre image de
très près pour bien voir les pixels et l'anticrénelage (*antialias*).

## 3 - Exploration de l'interface de Godot/Unity

**Remarque**: Tel qu'indiqué en classe, je me concentrerai sur le moteur de jeu
Godot. Si vous choisissez d'utiliser plutôt Unity, il faudrait plutôt remplacer
ces exercices par les mêmes sur Unity.

Familiarisez-vous avec l'interface de Godot. Je vous recommande de lire d'abord
les explications disponibles à http://docs.godotengine.org/en/stable/.

## 4 - Exportation-importation

**Remarque**: Dans le cas d'Unity, le format d'exportation/importation
recommandé est FBX. Il existe plusieurs tutoriels sur le sujet disponibles sur
Internet qui explique comment faire.

- Installez le greffon permettant d'exporter une scène au format Collada:
  https://github.com/godotengine/collada-exporter. N'oubliez pas de l'activer
  dans Blender.
- Assurez-vous d'avoir bien nommé tous les objets que vous avez créés à
  l'exercice précédent;
- Exportez la scène au format Collada et sauvegardez-le à un emplacement facile
  à retrouver.
- Dans Godot, importez la scène complète au format Collada.
- Expérimentez. Par exemple, vous pouvez modifier ou ajouter des éléments de la
  scène et l'exporter/importer à nouveau pour vous assurer que le tout
  fonctionne bien.
