# Laboratoire 5: Révision

Ce laboratoire tombe à un bon moment pour réviser certaines notions théoriques
abordées depuis le début du cours.

Je vous invite donc à tenter de compléter en partie ou en totalité les deux
anciens devoirs qui ont été réalisés par les étudiants du cours à l'automne
2015 et à l'automne 2016. Les énoncés sont disponibles dans les fichiers

- [Devoir 1, automne 2015](aut2015-devoir1.pdf)
- [Devoir 1, automne 2016](aut2016-devoir1.pdf)
